'''
This is the distribution of the active distributors over the years. The data is captured in the google sheet
https://docs.google.com/spreadsheets/d/1NDNR3k5M5JlFQJsCASLy8CU9bjHFDuYLZfIOBOtyzFw/edit


'''

import pymysql
import re

conn=pymysql.connect(host='localhost',user='root',password='mysqldbpasswordforrohitserver',db='reportsAnalysis')

for y in range(2012,2018):
    for m in range(1,13):
        if m<10:
            #print str(y)+"-0"+str(m)+"-31"
            dateString=str(y)+"-0"+str(m)+"-31"
        else:
            #print str(y) + "-" + str(m) + "-31"
            dateString=str(y)+"-"+str(m)+"-31"
        cursor = conn.cursor()
        sqlquery = "SELECT COUNT(*) FROM `distributorDetailsNew` WHERE `Created` <= '%s' AND " \
                   "(`Is_Active` = 1 OR `Modified` > '%s')" % (dateString,dateString)


        cursor.execute(sqlquery)
        result = cursor.fetchone()

        print(dateString , result[0])

