<?php
/**
This script will check if the reports ids that we have are dashboard reports or normal reports
If it is a dashboard report then it will try to findout the constiuent reports
This information is then saved in the table dashboardreports
**/



    require_once('SqlParser/PHPSQLParser.php' );
    set_time_limit(0);
    $db_hostname='bizom-access-logs.mysql.database.azure.com';
    $db_database='athena';
    $db_username='rohit@bizom-access-logs';
    $db_password='lkGPTa3eIpEGJqIR';

    $conn = new mysqli($db_hostname, $db_username, $db_password,"reportAnalysis");

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }


function insertTableNames($conn,$dashboardId,$reportId){

    $sqlQuery="INSERT IGNORE INTO `dashboardreports`(`dashboardId`, `reportId`) 
    VALUES ('".$dashboardId."','".trim($reportId)."')";
	echo $sqlQuery;
	
	// ('".$reportId."','".$queryLevel."','".$tableName."')";
    if ($conn->query($sqlQuery) === FALSE) {
            echo "Error: " . $sql . "<br>" . $conn->error;
    }
}



function getMostReadReports($conn){

	$arrReportIds=array();
	$sql="SELECT userRoles.company_id,company_url,company_name,role_id,accesslogs.report_id,COUNT( accesslogs.id ) as count FROM accesslogs
	 FORCE INDEX(created) LEFT JOIN users on users.id = accesslogs.user_id LEFT JOIN userRoles on userRoles.username = users.username 
	 WHERE accesslogs.created >= '2017-09-01 00:00:00' AND accesslogs.created <= '2017-09-31 23:59:59' and userRoles.user_id > 0 
	 GROUP by userRoles.role_id,accesslogs.report_id having count> 100 ORDER BY `count` DESC";
	
	$result = $conn->query($sql);
    $rows = $result->fetch_all();
	#print_r($rows);
	foreach ($rows as $row){
		array_push($arrReportIds,trim($row[4]));
	}
	return $arrReportIds;
}

function getReportData($conn,$reportId){

    $sql="Select query from reports where id='".$reportId."'";
    #echo $sql;
    $result = $conn->query($sql);
    $row = $result->fetch_row(); 

    return $row[0];
}



function getDashboardReports($conn,$arrReportIds)
{
    $arrDashboardReportIds=array();
    for($i=0;$i<sizeof($arrReportIds);$i++)
    {
        $reportData=getReportData($conn,$arrReportIds[$i]);
        $jsonObj=json_decode($reportData,true);
        if(!isset($jsonObj['query']['masterquery'])){
            array_push($arrDashboardReportIds,$arrReportIds[$i]);
        }


    }

    return $arrDashboardReportIds;
}

function strpos_all($haystack, $needle) {
    $offset = 0;
    $allpos = array();
    while (($pos = strpos($haystack, $needle, $offset)) !== FALSE) {
        $offset   = $pos + 1;
        $allpos[] = $pos;
    }
    return $allpos;
}


function getReportIdsFromDashboardSettings($conn,$dashboardId,$jsonObj)
{
    $arrDashboardReportIds=array();
    if(!isset($jsonObj['DashboardSettings']))
        return $arrDashboardReportIds;
    $dashboardSettings=$jsonObj['DashboardSettings']['initjscode'];
    echo "this si the dashboard";
    #print_r($dashboardSettings);
    // $ajsxPos=strpos()
    $urlString="https://reports.bizom.in/reports/viewreportJson/";
    $posUrls=strpos_all($dashboardSettings,$urlString);

    foreach($posUrls as $posUrl){
        #echo $posUrl."...";
        $urlSubstring=substr($dashboardSettings,strlen($urlString)+$posUrl,6);
        $reportIdEndPos=strpos($urlSubstring,'\'');
        $repoId=substr($urlSubstring,0,$reportIdEndPos);
        echo "ReportId found from dashboard widget is....".$repoId."\n";
        array_push($arrDashboardReportIds,$repoId);
        insertTableNames($conn,$dashboardId,$repoId);
    }

    return $arrDashboardReportIds; //it should be just tthe insert to the db

}

function getReportIdsFromWidgetSettings($conn,$dashboardId,$jsonObj)
{
    $arrDashboardReportIds=array();
    #print_r($jsonObj);
    if(!isset($jsonObj['WidgetSettings']))
        return $arrDashboardReportIds;
    $widgetSettings=$jsonObj['WidgetSettings'];
    
    foreach($widgetSettings as $widgetSetting)
    {
        #print_r($widgetSetting);
        //$widgetSetting['jscode'];
        if(isset($widgetSetting['jscode']))
        {
            // echo "inside";
            #print_r($widgetSetting['jscode']);
            $jscodeString=$widgetSetting['jscode'];
        
            $urlString="fetchdata(";
            $posUrls=strpos_all($jscodeString,$urlString);
            // echo "the posURLs";
            // print_r($posUrls);
            foreach($posUrls as $posUrl){
                echo "inside widgetsetting".$posUrl."...";
                $urlSubstring=substr($jscodeString,strlen($urlString)+$posUrl,20);
                echo $urlSubstring."\n";
                $reportIdEndPos=-1;
                $reportIdEndPos1=strpos($urlSubstring,')');
                
                $reportIdEndPos2=strpos($urlSubstring,',');
                if($reportIdEndPos2>0)
                    $reportIdEndPos=$reportIdEndPos2;
                else
                    $reportIdEndPos=$reportIdEndPos1;
                $repoId=substr($urlSubstring,0,$reportIdEndPos);
                echo $repoId;
                array_push($arrDashboardReportIds,$repoId);
                insertTableNames($conn,$dashboardId,$repoId);
            }
        }
    }
    return $arrDashboardReportIds; //it should be just tthe insert to the db


}


$arrReportIds=getMostReadReports($conn);
$arrDashboardReportIds=getDashboardReports($conn,$arrReportIds);

$arrDashboardReportIds=array_unique($arrDashboardReportIds);
$arrDashboardReportIds=array_values($arrDashboardReportIds);
print_r($arrDashboardReportIds);

$len=sizeof($arrDashboardReportIds);


/*// print_r($arrDashboardReportIds);
//$arrDashboardReportIds=array();
#$dashboardId=$argv[1];
*/
for($i=0;$i<$len;$i++)
{
    $dashboardId=$arrDashboardReportIds[$i];
    #$dashboardId=$argv[1];
    
    echo $dashboardId."\n";
    echo "Checking for report".$dashboardId."<br/>";
    $dashboardData=getReportData($conn,$dashboardId);
    // //print_r($dashboardData);

    $jsonObj=json_decode($dashboardData,true);
    // $arrDashboardReportIds=array();
    // //array_push($arrDashboardReportIds,
    getReportIdsFromDashboardSettings($conn,$dashboardId,$jsonObj);
    // //array_push($arrDashboardReportIds,
    getReportIdsFromWidgetSettings($conn,$dashboardId,$jsonObj);

}




?>
