''''
This script will fetch the active users distribution from the DB on the basis of month and role. This is the result that
is available to view through the google sheets https://docs.google.com/spreadsheets/d/1NDNR3k5M5JlFQJsCASLy8CU9bjHFDuYLZfIOBOtyzFw/edit

'''


import pymysql
import re

conn=pymysql.connect(host='localhost',user='root',password='mysqldbpasswordforrohitserver',db='reportsAnalysis')

for y in range(2013,2018):
    for m in range(1,13):
        if m<10:
            if m==2:
                dateString = str(y) + "-0" + str(m) + "-28"
            #print str(y)+"-0"+str(m)+"-31"
            dateString=str(y)+"-0"+str(m)+"-30"
        else:
            #print str(y) + "-" + str(m) + "-31"
            dateString=str(y)+"-"+str(m)+"-30"
        cursor = conn.cursor()
        sqlquery = "SELECT concat(MONTHNAME('%s'),'-',YEAR('%s')) as 'month__',COUNT(DISTINCT CASE WHEN " \
                   "userDetailsNew.Role_ID = 1 THEN concat(userId,'-',companyId) END) as " \
                   "'ROLE_ID1__',COUNT(DISTINCT CASE WHEN userDetailsNew.Role_ID = 2 THEN " \
                   "concat(userId,'-',companyId) END) as 'ROLE_ID2__'," \
                   "COUNT(DISTINCT CASE WHEN userDetailsNew.Role_ID = 3 THEN concat(userId,'-',companyId) END) " \
                   "as 'ROLE_ID3__',COUNT(DISTINCT CASE WHEN userDetailsNew.Role_ID = 4 " \
                   "THEN concat(userId,'-',companyId) END) as 'ROLE_ID4__'," \
                   "COUNT(DISTINCT CASE WHEN userDetailsNew.Role_ID = 5 THEN concat(userId,'-',companyId) END)" \
                   " as 'ROLE_ID5__',COUNT(DISTINCT CASE WHEN userDetailsNew.Role_ID = 6 THEN " \
                   "concat(userId,'-',companyId) END) as 'ROLE_ID6__',COUNT(DISTINCT CASE WHEN userDetailsNew.Role_ID = 7 " \
                   "THEN concat(userId,'-',companyId) END) as 'ROLE_ID7__'," \
                   "COUNT(DISTINCT CASE WHEN userDetailsNew.Role_ID = 8 THEN " \
                   "concat(userId,'-',companyId) END) as 'ROLE_ID8__'," \
                   "COUNT(DISTINCT CASE WHEN userDetailsNew.Role_ID > 8 THEN concat(userId,'-',companyId) END) as 'ROLE_IDOther__' " \
                   "FROM userDetailsNew WHERE userDetailsNew.Created <= '%s'" \
                   "AND (userDetailsNew.Status = 1 OR userDetailsNew.Inactive_Date > '%s') " \
                   "AND userDetailsNew.Role_ID > 0  AND YEAR(userDetailsNew.Created) >= 2013 " \
                   "GROUP BY month__" % (dateString,dateString,dateString,dateString)

        #print sqlquery
        cursor.execute(sqlquery)
        result = cursor.fetchone()

        print(dateString , result)

