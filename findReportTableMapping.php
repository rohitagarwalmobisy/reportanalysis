<?php
/**
This script will find out the various tables that the report is getting the data from
the script makes use o the PHPSQLParser script to do the actual parsing of the 
sql queries.
The output is saved to the reporttablemapping table

**/

require_once('SqlParser/PHPSQLParser.php' );
set_time_limit(0);
$db_hostname='bizom-access-logs.mysql.database.azure.com';
$db_database='athena';
$db_username='rohit@bizom-access-logs';
$db_password='lkGPTa3eIpEGJqIR';

$conn = new mysqli($db_hostname, $db_username, $db_password,"reportAnalysis");

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$arrListTables=array();

function getAllTableNames($conn,$arrListTables){
	
	$sql="select tableName from dbtablenames";
	$result = $conn->query($sql);
    $rows = $result->fetch_all();
	#print_r($rows);
	foreach ($rows as $row){
		array_push($arrListTables,trim($row[0]));
	}
	return $arrListTables;
}


function insertTableNames($conn,$reportId,$tableNames){

    $sqlQuery="INSERT IGNORE INTO `reporttablemapping`(`reportId`, `tableName`) 
    VALUES ";
	$len=sizeof($tableNames);
	echo "number of tables".$len;
	for($i=0;$i<$len;$i++)
	{
		echo "inserting table".$tableNames[$i];
		if($i<sizeof($tableNames)-1)
			$sqlQuery.="('".$reportId."','".$tableNames[$i]."'),";
		else
			$sqlQuery.="('".$reportId."','".$tableNames[$i]."');";

	}
	
	echo $sqlQuery;
	
	// ('".$reportId."','".$queryLevel."','".$tableName."')";
    if ($conn->query($sqlQuery) === FALSE) {
            echo "Error: " . $sql . "<br>" . $conn->error;
    }
}


function getMostReadReports($conn){

	$arrReportIds=array();
	$sql="SELECT userRoles.company_id,company_url,company_name,role_id,accesslogs.report_id,COUNT( accesslogs.id ) as count FROM accesslogs
	 FORCE INDEX(created) LEFT JOIN users on users.id = accesslogs.user_id LEFT JOIN userRoles on userRoles.username = users.username 
	 WHERE accesslogs.created >= '2017-09-01 00:00:00' AND accesslogs.created <= '2017-09-31 23:59:59' and userRoles.user_id > 0 
	 GROUP by userRoles.role_id,accesslogs.report_id having count> 100 ORDER BY `count` DESC";
	
	$result = $conn->query($sql);
    $rows = $result->fetch_all();
	#print_r($rows);
	foreach ($rows as $row){
		array_push($arrReportIds,trim($row[4]));
	}
	return $arrReportIds;
}


##get the report structure from the db
function getReportData($conn,$reportId){

    $sql="Select query from reports where id='".$reportId."'";
    #echo $sql;
    $result = $conn->query($sql);
    $row = $result->fetch_row(); 

    return $row[0];
}

function getMasterQuerySectionFromReportData($reportData){

	$jsonObj=json_decode($reportData,true);
	if(isset($jsonObj['query']['masterquery'])){
		 $masterQuery=$jsonObj['query']['masterquery'];
		 return $masterQuery;
	}
	else
		return null;
    #print_r($masterQuery);
    

}



#get the subquery sections from the report data
function getSubQuerySectionFromReportData($reportData){
	$jsonObj=json_decode($reportData,true);
	$subQueries='';
	if(isset($jsonObj['query']['subquery'])){

		$subQueries=$jsonObj['query']['subquery']['name'];
		
	}
    
	return $subQueries;
	
}


function extractFieldsFromParsedQuery( $parsedQuery ) {
	$response = array( "Result" => "true", "fields" => array(), "Error" => "" );
	if( !is_array( $parsedQuery ) ) {
		$response[ "Result" ] = "false";
		$response[ "Error" ] = "The query is probaly not a valid SQL query.";
	} else {
		if( isset( $parsedQuery['UNION'] )){
			$parsedQuery[ "SELECT" ] = $parsedQuery['UNION'][0][ "SELECT" ];
		}
		
		//SELECT
		if( isset( $parsedQuery[ "SELECT" ] ) ) {
			foreach( $parsedQuery[ "SELECT" ] as $selectFields ) {
				if( $selectFields[ "expr_type" ] == 'reserved' ) continue;
				$fields = array();
				$response[ "fields" ][] = getFieldNames( $selectFields, $fields );										
			}
		}
		
		//FROM
		if( isset( $parsedQuery[ "FROM" ] ) ) {
			foreach( $parsedQuery[ "FROM" ] as $fromFields ) {
				if( $fromFields[ 'expr_type' ] == 'table'  ) continue;
				$fields = array();
				if( $fromFields[ 'expr_type' ] == 'subquery' ) { 
					$response[ "fields" ][] = getFieldNamesFromSubQuery( $fromFields, $fields  );
				}
			}
		}
		
		//WHERE
		if( isset( $parsedQuery[ "WHERE" ] ) ) {
			foreach( $parsedQuery[ "WHERE" ] as $whereFields ) {
				if( $whereFields[ 'expr_type' ] == 'subquery' ) {
					$response[ "fields" ][] = getFieldNamesFromSubQuery( $whereFields, $fields  );
				} else {
					$fields = array();
					$response[ "fields" ][] =  getFieldNames( $whereFields, $fields );
				}
			}
		}

		//GROUP
		if( isset( $parsedQuery[ "GROUP" ] ) ) {
			foreach( $parsedQuery[ "GROUP" ] as $groupByFields ) {
				$fields = array();
				$response[ "fields" ][] =  getFieldNames( $groupByFields, $fields );	
			}
		}

		//HAVING
		if( isset( $parsedQuery[ "HAVING" ] ) ) {
			foreach( $parsedQuery[ "HAVING" ] as $havingByFields ) {
				$fields = array();
				$response[ "fields" ][] =  getFieldNames( $havingByFields, $fields );	
			}
		} 

		//ORDER
		if( isset( $parsedQuery[ "ORDER" ] ) ) {
			foreach( $parsedQuery[ "ORDER" ] as $orderByFields ) {
				$fields = array();
				$response[ "fields" ][] =  getFieldNames( $orderByFields, $fields );	
			}
		}
	}
	return $response;
}

function getFieldNames( $sqlParsedData, &$no_quotes ) {
	if( !empty( $sqlParsedData ) ) {
		$keys = array_keys( $sqlParsedData );
		if( in_array( 'no_quotes', $keys  ) ) {
			$no_quotes[] = $sqlParsedData[ 'no_quotes' ];
		} else {
			if( isset( $sqlParsedData[ 'sub_tree' ] ) && is_array( $sqlParsedData[ 'sub_tree' ] ) ) {
				foreach( $sqlParsedData[ 'sub_tree' ] as $sbuTree  ) {
					if( $sbuTree[ 'expr_type' ] == 'reserved' ) continue;
						getFieldNames( $sbuTree, $no_quotes );
				}
			}
		}	
	
	}
	return $no_quotes;
}

function getFieldNamesFromSubQuery( $subQuery, &$fields ) {
	if( !empty( $subQuery ) ) {
		foreach( $subQuery[ 'sub_tree' ] as $clauses  ) {
			foreach( $clauses as $data ) {
				if( isset( $data[ 'expr_type' ] ) && $data[ 'expr_type' ] == 'table' ) continue;
				if( isset( $data[ 'expr_type' ] ) && $data[ 'expr_type' ] == 'subquery' )  {
					$fields =  getFieldNamesFromSubQuery( $data, $fields );
				} else {
					$fields =  getFieldNames( $data, $fields );
				}
			}
		}
	}
	return $fields;
}


function getParsedTableNamesFromQueryString($parsedQuery,&$arrListTables){
	$arrayParsedTableNames=array();
	
	//this is the direct from . this will sometimes give the table names that are not coming from the fields
	//check the diff in reportid 2040..the table managerroles will be otherwise missing
	if( isset( $parsedQuery[ "FROM" ] ) ) {
		echo "FROM tables";
		foreach( $parsedQuery[ "FROM" ] as $fromFields ) {
			if( $fromFields[ 'expr_type' ] == 'table'  )
				if(isset($fromFields['table'])){
					echo $fromFields['table']."-->"."<br/>";
					array_push($arrayParsedTableNames,$fromFields['table']);
				}
				
		}
	}


	$extractedFields=extractFieldsFromParsedQuery($parsedQuery);
	#print_r($extractedFields);
	foreach($extractedFields['fields'] as $fields){
		foreach($fields as $field){
			$tableName=explode(".",$field)[0];
			array_push($arrayParsedTableNames,$tableName);

		}
	}

	$arrayUniqueParsedTableNames=array_unique($arrayParsedTableNames);
	print_r($arrayUniqueParsedTableNames);

	$finalTableNames=array();
	#print_r($arrListTables);
	###the names in the array needs to be checked with that in the list of tables in the DB
	foreach($arrayUniqueParsedTableNames as $item){
		if(in_array($item,$arrListTables))
			array_push($finalTableNames,$item);	

	}
	return $finalTableNames;
}

function getReportIdsFromDashboardReports($conn){
	$arrListTables=array();
	$sql="SELECT DISTINCT(reportId) FROM `dashboardreports`";
	$result = $conn->query($sql);
    $rows = $result->fetch_all();
	#print_r($rows);
	foreach ($rows as $row){
		array_push($arrListTables,trim($row[0]));
	}
	return $arrListTables;
}

/*
function parseReportData($reportData){
    $jsonObj=json_decode($reportData,true);
    $masterQuery=$jsonObj['query']['masterquery'];
    #print_r($masterQuery);
    return $masterQuery;
    //now we need to parse this query
    $queryString=strtolower($masterQuery);
    #print queryString
    #print "---"
    $startIndex= strpos($queryString,'from ');
    $endIndex= strpos($queryString,'where ');
    $tableFromString=substr($queryString,$startIndex+sizeof("from "),$endIndex);
    // tableArr=tableFromString.split(" join ")
    // #print tableArr ...we need to take care of the inner joins also ..check the 1report link to see what all tables are available
    // tableList=[]
    // for aa in tableArr:
    //      #incase the table name conatines \r\n replace it 
    //     aa=aa.strip()
    //     aa=aa.replace("\r\n"," ")
    //     tableName=aa.strip().split(" ")[0]
    //     if tableName.find("`on")>0:
    //         print "THERE IS A ISSUE",tableName
    //         tableName=tableName.replace("`on",'')
    //     tableName=tableName.replace('`','')
    //     tableName=tableName.replace('\'','')
    //     tableList.append(tableName)
    echo $tableFromString;



}
*/

/////MAIN CODE///
//get the mostly read reports
$arrReportIds=getMostReadReports($conn);
//get the reportids from the dashboard table also 
$arrReportIds=array_merge($arrReportIds,getReportIdsFromDashboardReports($conn));
$arrReportIds=array_unique($arrReportIds);
$arrReportIds=array_values($arrReportIds);
print_r($arrReportIds);
$len=sizeof($arrReportIds);
$fs=fopen("dashboards.txt","w");


//get the report id
#$reportId= $argv[1];
for($i=0;$i<$len;$i++)
{
	$reportId=$arrReportIds[$i];
	echo "fetching the data for report".$reportId."<br/>";
	//fetch the data for the report ig
	$reportData=getReportData($conn,$reportId);

	//make the list of the various database tables so that we can check if we have found the correct names and not some alias
	$arrListTables=getAllTableNames($conn,$arrListTables);


	//get the masterquery section from the report data
	$masterQueryData=getMasterQuerySectionFromReportData($reportData);
	if($masterQueryData==null)
	{
		fprintf($fs,$reportId);
		fprintf($fs,"\n");
		continue;
	}
		


	#insertTableNames($conn,12,"2","323");


	#initialize the parser
	$parser = new PHPSQLParser();

	//parse the master query section
	$parsedQuery=$parser->parse($masterQueryData);

	//array to contain the names of the table
	$finalTableNames=array();

	//get the names of the tables as declared in teh master query
	$finalTableNames=array_merge($finalTableNames,getParsedTableNamesFromQueryString($parsedQuery,$arrListTables));

	echo "master Tables";
	print_r($finalTableNames);

	//get the list if the subqueries in the report
	$subQueryArray=getSubQuerySectionFromReportData($reportData);
	echo "the number of subqueries".sizeof($subQueryArray);
	#print_r($subQueryArray);

	//if there are subqueries then parse each of the subquery to get the names of the table
	//and then merge them with the master table names
	if(sizeof($subQueryArray)>1)
	{
		foreach($subQueryArray as $subQuery){
			$parsedQuery=$parser->parse($subQuery);
			$finalTableNames=array_merge($finalTableNames,getParsedTableNamesFromQueryString($parsedQuery,$arrListTables));
			
			echo "subquery"."<br/>";
			print_r($finalTableNames);
		}
	}

	//finally we will remove the duplicate tables and create the final set of tables
	echo "the final output is";
	$finalUniqueTableNames=array_unique($finalTableNames);
	print_r($finalUniqueTableNames);
	$finalUniqueTableNames=array_values($finalUniqueTableNames);

	#insert the table names against the report id in the table

	insertTableNames($conn,$reportId,$finalUniqueTableNames);

}
fclose($fs);

$conn->close();


?>