'''
The script is to insert the data from a dump file to the db. The dates can pose problems while inserting. in case the excel is used,
make sure to check that the date is still in yyyy-mm-dd format
i think instead of using the plain comman separated csv split we should use the CSV reader
Note that we would first insert all the data assuming it is string. The actual datatypes are applied at the DB level
'''

import pymysql
import re
#read the csv file and then upload to the database
with open("UserWiseDumpReportFinal.csv") as f:
    content = f.readlines()

content = [x.split(',') for x in content]
# content=content.replace('"','')

conn=pymysql.connect(host='localhost',user='root',password='mysqldbpasswordforrohitserver',db='reportsAnalysis')

for c in content:
    c=[w.replace('"', '') for w in c]
    c = [w.replace('\r\n', '') for w in c]

    #print(c)
    conn = pymysql.connect(host='localhost', user='root', password='mysqldbpasswordforrohitserver',
                           db='reportsAnalysis')

    cursor = conn.cursor()
    sqlInsert = "INSERT IGNORE INTO `userDetailsNew` VALUES('%s','%s','%s','%s','%s','%s','%s','%s')" % \
                (c[0],c[1],c[2],c[3],c[4],c[5],c[6],c[7])

    #print(sqlInsert)
    cursor.execute(sqlInsert)
    conn.commit()
    conn.close()

